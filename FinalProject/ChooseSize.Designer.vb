﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ChooseSize
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbxBoardSize = New System.Windows.Forms.ComboBox()
        Me.lblChoose = New System.Windows.Forms.Label()
        Me.picSize = New System.Windows.Forms.PictureBox()
        Me.btnConfirm = New System.Windows.Forms.Button()
        CType(Me.picSize, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cbxBoardSize
        '
        Me.cbxBoardSize.BackColor = System.Drawing.Color.White
        Me.cbxBoardSize.FormattingEnabled = True
        Me.cbxBoardSize.Items.AddRange(New Object() {"Small (3 x 3)", "Medium (4 x 4)", "Large (5 x 5)", "Endurance (10 x 10)"})
        Me.cbxBoardSize.Location = New System.Drawing.Point(28, 69)
        Me.cbxBoardSize.Margin = New System.Windows.Forms.Padding(0)
        Me.cbxBoardSize.Name = "cbxBoardSize"
        Me.cbxBoardSize.Size = New System.Drawing.Size(154, 21)
        Me.cbxBoardSize.TabIndex = 0
        '
        'lblChoose
        '
        Me.lblChoose.AutoSize = True
        Me.lblChoose.BackColor = System.Drawing.Color.Transparent
        Me.lblChoose.Font = New System.Drawing.Font("Consolas", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChoose.ForeColor = System.Drawing.Color.White
        Me.lblChoose.Location = New System.Drawing.Point(12, 18)
        Me.lblChoose.Margin = New System.Windows.Forms.Padding(0)
        Me.lblChoose.Name = "lblChoose"
        Me.lblChoose.Size = New System.Drawing.Size(413, 37)
        Me.lblChoose.TabIndex = 1
        Me.lblChoose.Text = "Choose your board size"
        '
        'picSize
        '
        Me.picSize.BackColor = System.Drawing.Color.Transparent
        Me.picSize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picSize.Location = New System.Drawing.Point(224, 69)
        Me.picSize.Margin = New System.Windows.Forms.Padding(0)
        Me.picSize.Name = "picSize"
        Me.picSize.Size = New System.Drawing.Size(150, 150)
        Me.picSize.TabIndex = 2
        Me.picSize.TabStop = False
        '
        'btnConfirm
        '
        Me.btnConfirm.BackColor = System.Drawing.Color.Black
        Me.btnConfirm.Font = New System.Drawing.Font("Consolas", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConfirm.ForeColor = System.Drawing.Color.White
        Me.btnConfirm.Location = New System.Drawing.Point(28, 174)
        Me.btnConfirm.Margin = New System.Windows.Forms.Padding(0)
        Me.btnConfirm.Name = "btnConfirm"
        Me.btnConfirm.Size = New System.Drawing.Size(127, 45)
        Me.btnConfirm.TabIndex = 3
        Me.btnConfirm.Text = "Confirm"
        Me.btnConfirm.UseVisualStyleBackColor = False
        Me.btnConfirm.Visible = False
        '
        'ChooseSize
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(419, 236)
        Me.Controls.Add(Me.btnConfirm)
        Me.Controls.Add(Me.picSize)
        Me.Controls.Add(Me.lblChoose)
        Me.Controls.Add(Me.cbxBoardSize)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "ChooseSize"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Board Size"
        CType(Me.picSize, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxBoardSize As System.Windows.Forms.ComboBox
    Friend WithEvents lblChoose As System.Windows.Forms.Label
    Friend WithEvents picSize As System.Windows.Forms.PictureBox
    Friend WithEvents btnConfirm As System.Windows.Forms.Button
End Class
