﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GameBoard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picPlayer1 = New System.Windows.Forms.PictureBox()
        Me.picPlayer2 = New System.Windows.Forms.PictureBox()
        Me.Red1 = New System.Windows.Forms.PictureBox()
        Me.Red2 = New System.Windows.Forms.PictureBox()
        Me.Red3 = New System.Windows.Forms.PictureBox()
        Me.Red4 = New System.Windows.Forms.PictureBox()
        Me.Red5 = New System.Windows.Forms.PictureBox()
        Me.Red6 = New System.Windows.Forms.PictureBox()
        Me.Blue3 = New System.Windows.Forms.PictureBox()
        Me.Blue2 = New System.Windows.Forms.PictureBox()
        Me.Blue1 = New System.Windows.Forms.PictureBox()
        Me.Blue6 = New System.Windows.Forms.PictureBox()
        Me.Blue5 = New System.Windows.Forms.PictureBox()
        Me.Blue4 = New System.Windows.Forms.PictureBox()
        Me.RedCount = New System.Windows.Forms.Label()
        Me.BlueCount = New System.Windows.Forms.Label()
        CType(Me.picPlayer1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPlayer2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Red1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Red2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Red3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Red4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Red5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Red6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Blue3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Blue2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Blue1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Blue6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Blue5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Blue4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picPlayer1
        '
        Me.picPlayer1.BackColor = System.Drawing.Color.Transparent
        Me.picPlayer1.Image = Global.FinalProject.My.Resources.Resources.Player1
        Me.picPlayer1.Location = New System.Drawing.Point(12, 12)
        Me.picPlayer1.Margin = New System.Windows.Forms.Padding(0)
        Me.picPlayer1.Name = "picPlayer1"
        Me.picPlayer1.Size = New System.Drawing.Size(175, 75)
        Me.picPlayer1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picPlayer1.TabIndex = 62
        Me.picPlayer1.TabStop = False
        '
        'picPlayer2
        '
        Me.picPlayer2.BackColor = System.Drawing.Color.Transparent
        Me.picPlayer2.Image = Global.FinalProject.My.Resources.Resources.Player2
        Me.picPlayer2.Location = New System.Drawing.Point(582, 12)
        Me.picPlayer2.Margin = New System.Windows.Forms.Padding(0)
        Me.picPlayer2.Name = "picPlayer2"
        Me.picPlayer2.Size = New System.Drawing.Size(175, 75)
        Me.picPlayer2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picPlayer2.TabIndex = 63
        Me.picPlayer2.TabStop = False
        '
        'Red1
        '
        Me.Red1.BackColor = System.Drawing.Color.Transparent
        Me.Red1.BackgroundImage = Global.FinalProject.My.Resources.Resources.Black
        Me.Red1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Red1.Location = New System.Drawing.Point(41, 114)
        Me.Red1.Margin = New System.Windows.Forms.Padding(0)
        Me.Red1.Name = "Red1"
        Me.Red1.Size = New System.Drawing.Size(60, 60)
        Me.Red1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Red1.TabIndex = 71
        Me.Red1.TabStop = False
        '
        'Red2
        '
        Me.Red2.BackColor = System.Drawing.Color.Transparent
        Me.Red2.BackgroundImage = Global.FinalProject.My.Resources.Resources.Black
        Me.Red2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Red2.Location = New System.Drawing.Point(41, 186)
        Me.Red2.Margin = New System.Windows.Forms.Padding(0)
        Me.Red2.Name = "Red2"
        Me.Red2.Size = New System.Drawing.Size(60, 60)
        Me.Red2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Red2.TabIndex = 72
        Me.Red2.TabStop = False
        '
        'Red3
        '
        Me.Red3.BackColor = System.Drawing.Color.Transparent
        Me.Red3.BackgroundImage = Global.FinalProject.My.Resources.Resources.Black
        Me.Red3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Red3.Location = New System.Drawing.Point(41, 255)
        Me.Red3.Margin = New System.Windows.Forms.Padding(0)
        Me.Red3.Name = "Red3"
        Me.Red3.Size = New System.Drawing.Size(60, 60)
        Me.Red3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Red3.TabIndex = 73
        Me.Red3.TabStop = False
        '
        'Red4
        '
        Me.Red4.BackColor = System.Drawing.Color.Transparent
        Me.Red4.BackgroundImage = Global.FinalProject.My.Resources.Resources.Black
        Me.Red4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Red4.Location = New System.Drawing.Point(114, 114)
        Me.Red4.Margin = New System.Windows.Forms.Padding(0)
        Me.Red4.Name = "Red4"
        Me.Red4.Size = New System.Drawing.Size(60, 60)
        Me.Red4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Red4.TabIndex = 74
        Me.Red4.TabStop = False
        '
        'Red5
        '
        Me.Red5.BackColor = System.Drawing.Color.Transparent
        Me.Red5.BackgroundImage = Global.FinalProject.My.Resources.Resources.Black
        Me.Red5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Red5.Location = New System.Drawing.Point(114, 186)
        Me.Red5.Margin = New System.Windows.Forms.Padding(0)
        Me.Red5.Name = "Red5"
        Me.Red5.Size = New System.Drawing.Size(60, 60)
        Me.Red5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Red5.TabIndex = 75
        Me.Red5.TabStop = False
        '
        'Red6
        '
        Me.Red6.BackColor = System.Drawing.Color.Transparent
        Me.Red6.BackgroundImage = Global.FinalProject.My.Resources.Resources.Black
        Me.Red6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Red6.Location = New System.Drawing.Point(114, 255)
        Me.Red6.Margin = New System.Windows.Forms.Padding(0)
        Me.Red6.Name = "Red6"
        Me.Red6.Size = New System.Drawing.Size(60, 60)
        Me.Red6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Red6.TabIndex = 76
        Me.Red6.TabStop = False
        '
        'Blue3
        '
        Me.Blue3.BackColor = System.Drawing.Color.Transparent
        Me.Blue3.BackgroundImage = Global.FinalProject.My.Resources.Resources.Black
        Me.Blue3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Blue3.Location = New System.Drawing.Point(680, 255)
        Me.Blue3.Margin = New System.Windows.Forms.Padding(0)
        Me.Blue3.Name = "Blue3"
        Me.Blue3.Size = New System.Drawing.Size(60, 60)
        Me.Blue3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Blue3.TabIndex = 82
        Me.Blue3.TabStop = False
        '
        'Blue2
        '
        Me.Blue2.BackColor = System.Drawing.Color.Transparent
        Me.Blue2.BackgroundImage = Global.FinalProject.My.Resources.Resources.Black
        Me.Blue2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Blue2.Location = New System.Drawing.Point(680, 186)
        Me.Blue2.Margin = New System.Windows.Forms.Padding(0)
        Me.Blue2.Name = "Blue2"
        Me.Blue2.Size = New System.Drawing.Size(60, 60)
        Me.Blue2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Blue2.TabIndex = 81
        Me.Blue2.TabStop = False
        '
        'Blue1
        '
        Me.Blue1.BackColor = System.Drawing.Color.Transparent
        Me.Blue1.BackgroundImage = Global.FinalProject.My.Resources.Resources.Black
        Me.Blue1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Blue1.Location = New System.Drawing.Point(680, 114)
        Me.Blue1.Margin = New System.Windows.Forms.Padding(0)
        Me.Blue1.Name = "Blue1"
        Me.Blue1.Size = New System.Drawing.Size(60, 60)
        Me.Blue1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Blue1.TabIndex = 80
        Me.Blue1.TabStop = False
        '
        'Blue6
        '
        Me.Blue6.BackColor = System.Drawing.Color.Transparent
        Me.Blue6.BackgroundImage = Global.FinalProject.My.Resources.Resources.Black
        Me.Blue6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Blue6.Location = New System.Drawing.Point(607, 255)
        Me.Blue6.Margin = New System.Windows.Forms.Padding(0)
        Me.Blue6.Name = "Blue6"
        Me.Blue6.Size = New System.Drawing.Size(60, 60)
        Me.Blue6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Blue6.TabIndex = 79
        Me.Blue6.TabStop = False
        '
        'Blue5
        '
        Me.Blue5.BackColor = System.Drawing.Color.Transparent
        Me.Blue5.BackgroundImage = Global.FinalProject.My.Resources.Resources.Black
        Me.Blue5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Blue5.Location = New System.Drawing.Point(607, 186)
        Me.Blue5.Margin = New System.Windows.Forms.Padding(0)
        Me.Blue5.Name = "Blue5"
        Me.Blue5.Size = New System.Drawing.Size(60, 60)
        Me.Blue5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Blue5.TabIndex = 78
        Me.Blue5.TabStop = False
        '
        'Blue4
        '
        Me.Blue4.BackColor = System.Drawing.Color.Transparent
        Me.Blue4.BackgroundImage = Global.FinalProject.My.Resources.Resources.Black
        Me.Blue4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Blue4.Location = New System.Drawing.Point(607, 114)
        Me.Blue4.Margin = New System.Windows.Forms.Padding(0)
        Me.Blue4.Name = "Blue4"
        Me.Blue4.Size = New System.Drawing.Size(60, 60)
        Me.Blue4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Blue4.TabIndex = 77
        Me.Blue4.TabStop = False
        '
        'RedCount
        '
        Me.RedCount.AutoSize = True
        Me.RedCount.BackColor = System.Drawing.Color.Transparent
        Me.RedCount.Font = New System.Drawing.Font("Lucida Console", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RedCount.Location = New System.Drawing.Point(75, 342)
        Me.RedCount.Margin = New System.Windows.Forms.Padding(0)
        Me.RedCount.Name = "RedCount"
        Me.RedCount.Size = New System.Drawing.Size(68, 65)
        Me.RedCount.TabIndex = 83
        Me.RedCount.Text = "0"
        '
        'BlueCount
        '
        Me.BlueCount.AutoSize = True
        Me.BlueCount.BackColor = System.Drawing.Color.Transparent
        Me.BlueCount.Font = New System.Drawing.Font("Lucida Console", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BlueCount.Location = New System.Drawing.Point(641, 342)
        Me.BlueCount.Margin = New System.Windows.Forms.Padding(0)
        Me.BlueCount.Name = "BlueCount"
        Me.BlueCount.Size = New System.Drawing.Size(68, 65)
        Me.BlueCount.TabIndex = 84
        Me.BlueCount.Text = "0"
        '
        'GameBoard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImage = Global.FinalProject.My.Resources.Resources.CloudBGRed
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(767, 436)
        Me.Controls.Add(Me.BlueCount)
        Me.Controls.Add(Me.RedCount)
        Me.Controls.Add(Me.Blue3)
        Me.Controls.Add(Me.Blue2)
        Me.Controls.Add(Me.Blue1)
        Me.Controls.Add(Me.Blue6)
        Me.Controls.Add(Me.Blue5)
        Me.Controls.Add(Me.Blue4)
        Me.Controls.Add(Me.Red6)
        Me.Controls.Add(Me.Red5)
        Me.Controls.Add(Me.Red4)
        Me.Controls.Add(Me.Red3)
        Me.Controls.Add(Me.Red2)
        Me.Controls.Add(Me.Red1)
        Me.Controls.Add(Me.picPlayer2)
        Me.Controls.Add(Me.picPlayer1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "GameBoard"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Endurance Game"
        CType(Me.picPlayer1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPlayer2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Red1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Red2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Red3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Red4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Red5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Red6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Blue3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Blue2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Blue1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Blue6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Blue5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Blue4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picPlayer1 As System.Windows.Forms.PictureBox
    Friend WithEvents picPlayer2 As System.Windows.Forms.PictureBox
    Friend WithEvents Red1 As System.Windows.Forms.PictureBox
    Friend WithEvents Red2 As System.Windows.Forms.PictureBox
    Friend WithEvents Red3 As System.Windows.Forms.PictureBox
    Friend WithEvents Red4 As System.Windows.Forms.PictureBox
    Friend WithEvents Red5 As System.Windows.Forms.PictureBox
    Friend WithEvents Red6 As System.Windows.Forms.PictureBox
    Friend WithEvents Blue3 As System.Windows.Forms.PictureBox
    Friend WithEvents Blue2 As System.Windows.Forms.PictureBox
    Friend WithEvents Blue1 As System.Windows.Forms.PictureBox
    Friend WithEvents Blue6 As System.Windows.Forms.PictureBox
    Friend WithEvents Blue5 As System.Windows.Forms.PictureBox
    Friend WithEvents Blue4 As System.Windows.Forms.PictureBox
    Friend WithEvents RedCount As System.Windows.Forms.Label
    Friend WithEvents BlueCount As System.Windows.Forms.Label
End Class
