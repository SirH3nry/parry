﻿Public Class MainMenu
    Private Sub btnQuit_Click(sender As System.Object, e As System.EventArgs) Handles btnQuit.Click
        Me.Close()
    End Sub

    Private Sub btnNew_Click(sender As System.Object, e As System.EventArgs) Handles btnNew.Click
        Me.Hide()
        ChooseSize.ShowDialog()
        Me.Close()
    End Sub

    Private Sub btnHow_Click(sender As System.Object, e As System.EventArgs) Handles btnHow.Click
        Tutorial.Show()
        Me.Close()
    End Sub
End Class