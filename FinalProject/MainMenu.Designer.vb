﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picMainMenu = New System.Windows.Forms.PictureBox()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.btnHow = New System.Windows.Forms.Button()
        Me.btnQuit = New System.Windows.Forms.Button()
        CType(Me.picMainMenu, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picMainMenu
        '
        Me.picMainMenu.BackColor = System.Drawing.Color.Transparent
        Me.picMainMenu.Image = Global.FinalProject.My.Resources.Resources.MainMenu
        Me.picMainMenu.Location = New System.Drawing.Point(315, 137)
        Me.picMainMenu.Margin = New System.Windows.Forms.Padding(0)
        Me.picMainMenu.Name = "picMainMenu"
        Me.picMainMenu.Size = New System.Drawing.Size(225, 53)
        Me.picMainMenu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picMainMenu.TabIndex = 0
        Me.picMainMenu.TabStop = False
        '
        'btnNew
        '
        Me.btnNew.BackColor = System.Drawing.Color.Black
        Me.btnNew.BackgroundImage = Global.FinalProject.My.Resources.Resources.NewGame
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnNew.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatAppearance.CheckedBackColor = System.Drawing.Color.Black
        Me.btnNew.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black
        Me.btnNew.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black
        Me.btnNew.ForeColor = System.Drawing.Color.Transparent
        Me.btnNew.Location = New System.Drawing.Point(315, 263)
        Me.btnNew.Margin = New System.Windows.Forms.Padding(0)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(225, 50)
        Me.btnNew.TabIndex = 1
        Me.btnNew.UseVisualStyleBackColor = False
        '
        'btnHow
        '
        Me.btnHow.BackColor = System.Drawing.Color.Black
        Me.btnHow.BackgroundImage = Global.FinalProject.My.Resources.Resources.HowToPlay
        Me.btnHow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHow.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnHow.FlatAppearance.BorderSize = 0
        Me.btnHow.FlatAppearance.CheckedBackColor = System.Drawing.Color.Black
        Me.btnHow.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black
        Me.btnHow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black
        Me.btnHow.Location = New System.Drawing.Point(315, 319)
        Me.btnHow.Margin = New System.Windows.Forms.Padding(0)
        Me.btnHow.Name = "btnHow"
        Me.btnHow.Size = New System.Drawing.Size(225, 50)
        Me.btnHow.TabIndex = 2
        Me.btnHow.UseVisualStyleBackColor = False
        '
        'btnQuit
        '
        Me.btnQuit.BackColor = System.Drawing.Color.Black
        Me.btnQuit.BackgroundImage = Global.FinalProject.My.Resources.Resources.Quit
        Me.btnQuit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnQuit.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnQuit.FlatAppearance.BorderSize = 0
        Me.btnQuit.FlatAppearance.CheckedBackColor = System.Drawing.Color.Black
        Me.btnQuit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black
        Me.btnQuit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black
        Me.btnQuit.Location = New System.Drawing.Point(370, 375)
        Me.btnQuit.Margin = New System.Windows.Forms.Padding(0)
        Me.btnQuit.Name = "btnQuit"
        Me.btnQuit.Size = New System.Drawing.Size(115, 50)
        Me.btnQuit.TabIndex = 3
        Me.btnQuit.UseVisualStyleBackColor = False
        '
        'MainMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImage = Global.FinalProject.My.Resources.Resources.CloudBGWhite
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(870, 556)
        Me.Controls.Add(Me.btnQuit)
        Me.Controls.Add(Me.btnHow)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.picMainMenu)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "MainMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Main Menu"
        CType(Me.picMainMenu, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picMainMenu As System.Windows.Forms.PictureBox
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents btnHow As System.Windows.Forms.Button
    Friend WithEvents btnQuit As System.Windows.Forms.Button

End Class
