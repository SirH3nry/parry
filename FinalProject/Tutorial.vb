﻿Public Class Tutorial
    Public nipples As UShort = 1
    Private Sub Tutorial_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        picTut.Image = Image.FromFile(My.Application.Info.DirectoryPath & "\Tutorials\tut" & nipples & ".png")
    End Sub
    Private Sub onClose() Handles Me.FormClosed
        MainMenu.Show()
    End Sub

    Private Sub btnCont_Click(sender As System.Object, e As System.EventArgs) Handles btnCont.Click
        nipples = nipples + 1
        If nipples = 13 Then
            Me.Close()
        Else
            picTut.Image = Image.FromFile(My.Application.Info.DirectoryPath & "\Tutorials\tut" & nipples & ".png")
        End If
    End Sub
End Class