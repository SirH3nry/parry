﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Tutorial
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picTut = New System.Windows.Forms.PictureBox()
        Me.btnCont = New System.Windows.Forms.Button()
        CType(Me.picTut, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picTut
        '
        Me.picTut.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picTut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.picTut.Location = New System.Drawing.Point(0, 0)
        Me.picTut.Margin = New System.Windows.Forms.Padding(0)
        Me.picTut.Name = "picTut"
        Me.picTut.Size = New System.Drawing.Size(895, 713)
        Me.picTut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picTut.TabIndex = 0
        Me.picTut.TabStop = False
        '
        'btnCont
        '
        Me.btnCont.BackgroundImage = Global.FinalProject.My.Resources.Resources.Black
        Me.btnCont.FlatAppearance.BorderSize = 0
        Me.btnCont.Font = New System.Drawing.Font("Comic Sans MS", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCont.ForeColor = System.Drawing.Color.CadetBlue
        Me.btnCont.Location = New System.Drawing.Point(342, 717)
        Me.btnCont.Margin = New System.Windows.Forms.Padding(0)
        Me.btnCont.Name = "btnCont"
        Me.btnCont.Size = New System.Drawing.Size(186, 54)
        Me.btnCont.TabIndex = 1
        Me.btnCont.Text = "Continue"
        Me.btnCont.UseVisualStyleBackColor = True
        '
        'Tutorial
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(894, 776)
        Me.Controls.Add(Me.btnCont)
        Me.Controls.Add(Me.picTut)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "Tutorial"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "How to Play!"
        CType(Me.picTut, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picTut As System.Windows.Forms.PictureBox
    Friend WithEvents btnCont As System.Windows.Forms.Button
End Class
