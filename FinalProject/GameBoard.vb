﻿Public Class GameBoard
    'TO DO:
    'Randomly generate which player goes first
    'Cloud color background corresponds to player turn (red/blue)

    Public turn As Integer
    Public R As New Random
    Public Board(99) As String
    Public BlueSelected As Integer = 0
    Public RedSelected As Integer = 0
    Public RedPieces(5) As String
    Public BluePieces(5) As String
    Public grid(100) As PictureBox
    Public gridWidth As Integer

    Private Sub GameBoard_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim gameGrid(ChooseSize.num * ChooseSize.num) As PictureBox
        Dim xInterval As Integer = 66
        Dim yInterval As Integer = 0
        turn = R.Next(0, 2)
        gridWidth = ChooseSize.num

        If turn = 0 Then
            Me.BackgroundImage = My.Resources.CloudBGRed
        Else
            Me.BackgroundImage = My.Resources.CloudBGBlue
        End If

        'Dynamically sets the location of everything:
        picPlayer2.Location = New Point(Me.Width - (12 + picPlayer2.Width), 12)
        Red1.Location = New Point(12, 110)
        Red2.Location = New Point(12, 176)
        Red3.Location = New Point(12, 242)
        Red4.Location = New Point(78, 110)
        Red5.Location = New Point(78, 176)
        Red6.Location = New Point(78, 242)
        Blue1.Location = New Point(Me.Width - 72, 110)
        Blue2.Location = New Point(Me.Width - 72, 176)
        Blue3.Location = New Point(Me.Width - 72, 242)
        Blue4.Location = New Point(Me.Width - 138, 110)
        Blue5.Location = New Point(Me.Width - 138, 176)
        Blue6.Location = New Point(Me.Width - 138, 242)
        RedCount.Location = New Point(45, 340)
        BlueCount.Location = New Point(Me.Width - (BlueCount.Width + 45), 340)

        For c = 0 To 5
            Dim s As String
            If R.Next(0, 2) = 0 Then
                s = "S"
            Else
                s = "H"
            End If
            RedPieces(c) = "R" & s & R.Next(1, 10)

            If R.Next(0, 2) = 0 Then
                s = "S"
            Else
                s = "H"
            End If
            BluePieces(c) = "B" & s & R.Next(1, 10)
        Next

        For i = 0 To 99
            Board(i) = "Black"
        Next

        Red1.Image = Image.FromFile(My.Application.Info.DirectoryPath & "\Pieces\" & RedPieces(0) & ".png")
        Red2.Image = Image.FromFile(My.Application.Info.DirectoryPath & "\Pieces\" & RedPieces(1) & ".png")
        Red3.Image = Image.FromFile(My.Application.Info.DirectoryPath & "\Pieces\" & RedPieces(2) & ".png")
        Red4.Image = Image.FromFile(My.Application.Info.DirectoryPath & "\Pieces\" & RedPieces(3) & ".png")
        Red5.Image = Image.FromFile(My.Application.Info.DirectoryPath & "\Pieces\" & RedPieces(4) & ".png")
        Red6.Image = Image.FromFile(My.Application.Info.DirectoryPath & "\Pieces\" & RedPieces(5) & ".png")
        Blue1.Image = Image.FromFile(My.Application.Info.DirectoryPath & "\Pieces\" & BluePieces(0) & ".png")
        Blue2.Image = Image.FromFile(My.Application.Info.DirectoryPath & "\Pieces\" & BluePieces(1) & ".png")
        Blue3.Image = Image.FromFile(My.Application.Info.DirectoryPath & "\Pieces\" & BluePieces(2) & ".png")
        Blue4.Image = Image.FromFile(My.Application.Info.DirectoryPath & "\Pieces\" & BluePieces(3) & ".png")
        Blue5.Image = Image.FromFile(My.Application.Info.DirectoryPath & "\Pieces\" & BluePieces(4) & ".png")
        Blue6.Image = Image.FromFile(My.Application.Info.DirectoryPath & "\Pieces\" & BluePieces(5) & ".png")

        'Creates all of the game board pieces
        For i As UInteger = 0 To ((ChooseSize.num * ChooseSize.num) - 1)
            gameGrid(i) = New PictureBox
            With gameGrid(i)
                If i < 10 Then
                    .Name = "G0" & CStr(i)
                Else
                    .Name = "G" & CStr(i)
                End If
                .Visible = True
                .Enabled = True
                .BackColor = Color.Transparent
                .BackgroundImage = My.Resources.Black
                .BackgroundImageLayout = ImageLayout.Stretch
                .Width = 60
                .Height = 60
                .SizeMode = PictureBoxSizeMode.StretchImage
                .Location = New Point((Me.Width - ((ChooseSize.num * 66) - 6)) / 2 + xInterval * (i Mod ChooseSize.num), 120 + yInterval)
                If ((i Mod ChooseSize.num) = (ChooseSize.num - 1)) Then
                    yInterval += 66
                End If
                grid(i) = gameGrid(i)
                Me.Controls.Add(grid(i))
                AddHandler grid(i).Click, AddressOf GameLogic
            End With
        Next
    End Sub

    Private Sub GameLogic(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim s As String = sender.Name.ToString
        Dim index As Integer = 10 * Val(s.Chars(1)) + Val(s.Chars(2))
        Dim red, blue As Integer
        Dim black As Integer = gridWidth * gridWidth
        If (RedSelected <> 0 And Board(index) = "Black") Then
            ' sender.Image = Image.FromFile(
            'My.Application.Info.DirectoryPath & "\Pieces\" & RedPieces(RedSelected - 1) & ".png")
            Board(index) = RedPieces(RedSelected - 1)
            Red1.BackgroundImage = My.Resources.Black
            Red2.BackgroundImage = My.Resources.Black
            Red3.BackgroundImage = My.Resources.Black
            Red4.BackgroundImage = My.Resources.Black
            Red5.BackgroundImage = My.Resources.Black
            Red6.BackgroundImage = My.Resources.Black
            RedPieces(RedSelected - 1) = "Black"
            NextTurn()
            MakeChanges(sender)
            UpdateVisuals()
            RedSelected = 0
        ElseIf (BlueSelected <> 0 And Board(index) = "Black") Then
            'sender.Image = Image.FromFile(
            'My.Application.Info.DirectoryPath & "\Pieces\" & BluePieces(BlueSelected - 1) & ".png")
            Board(index) = BluePieces(BlueSelected - 1)
            Blue1.BackgroundImage = My.Resources.Black
            Blue2.BackgroundImage = My.Resources.Black
            Blue3.BackgroundImage = My.Resources.Black
            Blue4.BackgroundImage = My.Resources.Black
            Blue5.BackgroundImage = My.Resources.Black
            Blue6.BackgroundImage = My.Resources.Black
            BluePieces(BlueSelected - 1) = "Black"
            NextTurn()
            MakeChanges(sender)
            UpdateVisuals()
            BlueSelected = 0
        End If

        red = 0
        blue = 0

        For i = 0 To (gridWidth * gridWidth) - 1
            If Board(i) = "Black" Then
            ElseIf Board(i).Chars(0) = "B" Then
                If Board(i).Chars(2) = "A" Then
                    blue = blue + 2
                Else
                    blue = blue + 1
                End If
                black = black - 1
            Else
                If Board(i).Chars(2) = "A" Then
                    red = red + 2
                Else
                    red = red + 1
                End If
                black = black - 1
            End If
        Next

        BlueCount.Text = CStr(blue)
        RedCount.Text = CStr(red)
        
            If black = 0 Then
                If Val(RedCount.Text) > Val(BlueCount.Text) Then
                    MessageBox.Show("Red Wins!")
                ElseIf Val(RedCount.Text) < Val(BlueCount.Text) Then
                    MessageBox.Show("Blue Wins!")
                Else
                    MessageBox.Show("Draw.")
                End If
            End If

    End Sub

    Public Sub SelectRedPiece(ByVal sender As System.Object)
        If turn = 0 Then
            Red1.BackgroundImage = My.Resources.Black
            Red2.BackgroundImage = My.Resources.Black
            Red3.BackgroundImage = My.Resources.Black
            Red4.BackgroundImage = My.Resources.Black
            Red5.BackgroundImage = My.Resources.Black
            Red6.BackgroundImage = My.Resources.Black
            sender.BackgroundImage = My.Resources.SelectRed
            Dim s As String = sender.Name.ToString
            RedSelected = Val(s.Chars(3))
        End If
    End Sub
    Public Sub SelectBluePiece(ByVal sender As System.Object)
        If turn = 1 Then
            Blue1.BackgroundImage = My.Resources.Black
            Blue2.BackgroundImage = My.Resources.Black
            Blue3.BackgroundImage = My.Resources.Black
            Blue4.BackgroundImage = My.Resources.Black
            Blue5.BackgroundImage = My.Resources.Black
            Blue6.BackgroundImage = My.Resources.Black
            sender.BackgroundImage = My.Resources.SelectBlue
            Dim s As String = sender.Name.ToString
            BlueSelected = Val(s.Chars(4))
        End If
    End Sub
    Private Sub Red1_Click(sender As System.Object, e As System.EventArgs) Handles Red1.Click
        If RedPieces(0) <> "Black" Then
            SelectRedPiece(Red1)
        End If
    End Sub
    Private Sub Red2_Click(sender As System.Object, e As System.EventArgs) Handles Red2.Click
        If RedPieces(1) <> "Black" Then
            SelectRedPiece(Red2)
        End If
    End Sub
    Private Sub Red3_Click(sender As System.Object, e As System.EventArgs) Handles Red3.Click
        If RedPieces(2) <> "Black" Then
            SelectRedPiece(Red3)
        End If
    End Sub
    Private Sub Red4_Click(sender As System.Object, e As System.EventArgs) Handles Red4.Click
        If RedPieces(3) <> "Black" Then
            SelectRedPiece(Red4)
        End If
    End Sub
    Private Sub Red5_Click(sender As System.Object, e As System.EventArgs) Handles Red5.Click
        If RedPieces(4) <> "Black" Then
            SelectRedPiece(Red5)
        End If
    End Sub
    Private Sub Red6_Click(sender As System.Object, e As System.EventArgs) Handles Red6.Click
        If RedPieces(5) <> "Black" Then
            SelectRedPiece(Red6)
        End If
    End Sub
    Private Sub Blue4_Click(sender As System.Object, e As System.EventArgs) Handles Blue4.Click
        If BluePieces(3) <> "Black" Then
            SelectBluePiece(Blue4)
        End If
    End Sub
    Private Sub Blue5_Click(sender As System.Object, e As System.EventArgs) Handles Blue5.Click
        If BluePieces(4) <> "Black" Then
            SelectBluePiece(Blue5)
        End If
    End Sub
    Private Sub Blue6_Click(sender As System.Object, e As System.EventArgs) Handles Blue6.Click
        If BluePieces(5) <> "Black" Then
            SelectBluePiece(Blue6)
        End If
    End Sub
    Private Sub Blue1_Click(sender As System.Object, e As System.EventArgs) Handles Blue1.Click
        If BluePieces(0) <> "Black" Then
            SelectBluePiece(Blue1)
        End If
    End Sub
    Private Sub Blue2_Click(sender As System.Object, e As System.EventArgs) Handles Blue2.Click
        If BluePieces(1) <> "Black" Then
            SelectBluePiece(Blue2)
        End If
    End Sub
    Private Sub Blue3_Click(sender As System.Object, e As System.EventArgs) Handles Blue3.Click
        If BluePieces(2) <> "Black" Then
            SelectBluePiece(Blue3)
        End If
    End Sub

    Private Sub NextTurn()
        Dim s As String
        If turn = 0 Then
            BluePieces(3) = BluePieces(0)
            BluePieces(4) = BluePieces(1)
            BluePieces(5) = BluePieces(2)
            For c = 0 To 2
                If R.Next(0, 2) = 0 Then
                    s = "S"
                Else
                    s = "H"
                End If
                BluePieces(c) = "B" & s & R.Next(1, 10)
            Next
            Me.BackgroundImage = My.Resources.CloudBGBlue
            Red1.BackgroundImage = My.Resources.Black
            Red2.BackgroundImage = My.Resources.Black
            Red3.BackgroundImage = My.Resources.Black
            Red4.BackgroundImage = My.Resources.Black
            Red5.BackgroundImage = My.Resources.Black
            Red6.BackgroundImage = My.Resources.Black
            turn = 1
        ElseIf turn = 1 Then
            RedPieces(3) = RedPieces(0)
            RedPieces(4) = RedPieces(1)
            RedPieces(5) = RedPieces(2)
            For c = 0 To 2
                If R.Next(0, 2) = 0 Then
                    s = "S"
                Else
                    s = "H"
                End If
                RedPieces(c) = "R" & s & R.Next(1, 10)
            Next
            Me.BackgroundImage = My.Resources.CloudBGRed
            Blue1.BackgroundImage = My.Resources.Black
            Blue2.BackgroundImage = My.Resources.Black
            Blue3.BackgroundImage = My.Resources.Black
            Blue4.BackgroundImage = My.Resources.Black
            Blue5.BackgroundImage = My.Resources.Black
            Blue6.BackgroundImage = My.Resources.Black

            turn = 0
        End If
    End Sub
    Private Sub UpdateVisuals()
        Red1.Image = Image.FromFile(
          My.Application.Info.DirectoryPath & "\Pieces\" & RedPieces(0) & ".png")
        Red2.Image = Image.FromFile(
          My.Application.Info.DirectoryPath & "\Pieces\" & RedPieces(1) & ".png")
        Red3.Image = Image.FromFile(
          My.Application.Info.DirectoryPath & "\Pieces\" & RedPieces(2) & ".png")
        Red4.Image = Image.FromFile(
          My.Application.Info.DirectoryPath & "\Pieces\" & RedPieces(3) & ".png")
        Red5.Image = Image.FromFile(
          My.Application.Info.DirectoryPath & "\Pieces\" & RedPieces(4) & ".png")
        Red6.Image = Image.FromFile(
          My.Application.Info.DirectoryPath & "\Pieces\" & RedPieces(5) & ".png")
        Blue1.Image = Image.FromFile(
          My.Application.Info.DirectoryPath & "\Pieces\" & BluePieces(0) & ".png")
        Blue2.Image = Image.FromFile(
          My.Application.Info.DirectoryPath & "\Pieces\" & BluePieces(1) & ".png")
        Blue3.Image = Image.FromFile(
          My.Application.Info.DirectoryPath & "\Pieces\" & BluePieces(2) & ".png")
        Blue4.Image = Image.FromFile(
          My.Application.Info.DirectoryPath & "\Pieces\" & BluePieces(3) & ".png")
        Blue5.Image = Image.FromFile(
          My.Application.Info.DirectoryPath & "\Pieces\" & BluePieces(4) & ".png")
        Blue6.Image = Image.FromFile(
          My.Application.Info.DirectoryPath & "\Pieces\" & BluePieces(5) & ".png")

        For i = 0 To ((gridWidth * gridWidth) - 1)
            grid(i).Image = Image.FromFile(
                My.Application.Info.DirectoryPath & "\Pieces\" & Board(i) & ".png")
        Next
    End Sub
    Private Sub MakeChanges(ByVal sender As System.Object)
        Dim s As String = sender.Name.ToString
        Dim index As Integer = 10 * Val(s.Chars(1)) + Val(s.Chars(2))
        Dim numCurrent, numRight, numLeft, numUp, numDown As Integer
        numCurrent = Val(Board(index).Chars(2))

        If (index = 0) Then  'Top Left Corner

            If Board(index + 1).Chars(2) = "A" Then
                numRight = 10
            Else
                numRight = getNumber(index + 1)
            End If
            If Board(index + gridWidth).Chars(2) = "A" Then
                numDown = 10
            Else
                numDown = getNumber(index + gridWidth)
            End If

            If Board(index).Chars(1) = "S" Then 'Sword Logic
                If (Board(index + 1).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numRight _
                  And Board(index + 1) <> "Black" Then
                    If (Board(index + 1).Chars(0) = "B") Then
                        Board(index + 1) = "R" & Board(index + 1).Chars(1) & Board(index + 1).Chars(2)
                    Else
                        Board(index + 1) = "B" & Board(index + 1).Chars(1) & Board(index + 1).Chars(2)
                    End If
                Else

                End If

                If (Board(index + gridWidth).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numDown _
                  And Board(index + gridWidth) <> "Black" Then
                    If (Board(index + gridWidth).Chars(0) = "B") Then
                        Board(index + gridWidth) = "R" & Board(index + gridWidth).Chars(1) _
                          & Board(index + gridWidth).Chars(2)
                    Else
                        Board(index + gridWidth) = "B" & Board(index + gridWidth).Chars(1) _
                          & Board(index + gridWidth).Chars(2)
                    End If
                End If
            Else 'Shield Logic
                If Board(index + 1).Chars(0) <> Board(index).Chars(0) And Board(index + 1) <> "Black" Then
                    If Board(index + 1).Chars(2) <> "A" And Board(index + 1).Chars(2) <> "1" Then
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & Val(Board(index + 1).Chars(2)) - 1
                    ElseIf Board(index + 1).Chars(2) = "1" Then

                    Else
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index + 1).Chars(0) = Board(index).Chars(0) And Board(index + 1) <> "Black" Then
                    If Board(index + 1).Chars(2) = "A" Then

                    ElseIf Board(index + 1).Chars(2) = "9" Then
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & "A"
                    Else
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & Val(Board(index + 1).Chars(2)) + 1
                    End If
                End If

                If Board(index + gridWidth).Chars(0) <> Board(index).Chars(0) And Board(index + gridWidth) <> "Black" Then
                    If Board(index + gridWidth).Chars(2) <> "A" And Board(index + gridWidth).Chars(2) <> "1" Then
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & Val(Board(index + gridWidth).Chars(2)) - 1
                    ElseIf Board(index + gridWidth).Chars(2) = "1" Then

                    Else
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index + gridWidth).Chars(0) = Board(index).Chars(0) And Board(index + gridWidth) <> "Black" Then
                    If Board(index + gridWidth).Chars(2) = "A" Then

                    ElseIf Board(index + gridWidth).Chars(2) = "9" Then
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & "A"
                    Else
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & Val(Board(index + gridWidth).Chars(2)) + 1
                    End If
                End If

            End If


        ElseIf (index = gridWidth - 1) Then   'Top Right Corner
            If Board(index - 1).Chars(2) = "A" Then
                numLeft = 10
            Else
                numLeft = getNumber(index - 1)
            End If
            If Board(index + gridWidth).Chars(2) = "A" Then
                numDown = 10
            Else
                numDown = getNumber(index + gridWidth)
            End If

            If Board(index).Chars(1) = "S" Then

                If (Board(index - 1).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numLeft _
                  And Board(index - 1) <> "Black" Then
                    If (Board(index - 1).Chars(0) = "B") Then
                        Board(index - 1) = "R" & Board(index - 1).Chars(1) & Board(index - 1).Chars(2)
                    Else
                        Board(index - 1) = "B" & Board(index - 1).Chars(1) & Board(index - 1).Chars(2)
                    End If
                Else

                End If

                If (Board(index + gridWidth).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numDown _
                  And Board(index + gridWidth) <> "Black" Then
                    If (Board(index + gridWidth).Chars(0) = "B") Then
                        Board(index + gridWidth) = "R" & Board(index + gridWidth).Chars(1) _
                          & Board(index + gridWidth).Chars(2)
                    Else
                        Board(index + gridWidth) = "B" & Board(index + gridWidth).Chars(1) _
                          & Board(index + gridWidth).Chars(2)
                    End If
                End If
            Else
                If Board(index - 1).Chars(0) <> Board(index).Chars(0) And Board(index - 1) <> "Black" Then
                    If Board(index - 1).Chars(2) <> "A" And Board(index - 1).Chars(2) <> "1" Then
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & Val(Board(index - 1).Chars(2)) - 1
                    ElseIf Board(index - 1).Chars(2) = "1" Then

                    Else
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index - 1).Chars(0) = Board(index).Chars(0) And Board(index - 1) <> "Black" Then
                    If Board(index - 1).Chars(2) = "A" Then

                    ElseIf Board(index - 1).Chars(2) = "9" Then
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & "A"
                    Else
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & Val(Board(index - 1).Chars(2)) + 1
                    End If
                End If

                If Board(index + gridWidth).Chars(0) <> Board(index).Chars(0) And Board(index + gridWidth) <> "Black" Then
                    If Board(index + gridWidth).Chars(2) <> "A" And Board(index + gridWidth).Chars(2) <> "1" Then
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & Val(Board(index + gridWidth).Chars(2)) - 1
                    ElseIf Board(index + gridWidth).Chars(2) = "1" Then

                    Else
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index + gridWidth).Chars(0) = Board(index).Chars(0) And Board(index + gridWidth) <> "Black" Then
                    If Board(index + gridWidth).Chars(2) = "A" Then

                    ElseIf Board(index + gridWidth).Chars(2) = "9" Then
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & "A"
                    Else
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & Val(Board(index + gridWidth).Chars(2)) + 1
                    End If
                End If

            End If

        ElseIf (index = (gridWidth * gridWidth) - 1) Then   'Bottom Right Corner
            If Board(index - 1).Chars(2) = "A" Then
                numLeft = 10
            Else
                numLeft = getNumber(index - 1)
            End If
            If Board(index - gridWidth).Chars(2) = "A" Then
                numUp = 10
            Else
                numUp = getNumber(index - gridWidth)
            End If

            If Board(index).Chars(1) = "S" Then

                If (Board(index - 1).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numLeft _
                  And Board(index - 1) <> "Black" Then
                    If (Board(index - 1).Chars(0) = "B") Then
                        Board(index - 1) = "R" & Board(index - 1).Chars(1) & Board(index - 1).Chars(2)
                    Else
                        Board(index - 1) = "B" & Board(index - 1).Chars(1) & Board(index - 1).Chars(2)
                    End If
                Else

                End If

                If (Board(index - gridWidth).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numUp _
                  And Board(index - gridWidth) <> "Black" Then
                    If (Board(index - gridWidth).Chars(0) = "B") Then
                        Board(index - gridWidth) = "R" & Board(index - gridWidth).Chars(1) _
                          & Board(index - gridWidth).Chars(2)
                    Else
                        Board(index - gridWidth) = "B" & Board(index - gridWidth).Chars(1) _
                          & Board(index - gridWidth).Chars(2)
                    End If
                End If

            Else
                If Board(index - 1).Chars(0) <> Board(index).Chars(0) And Board(index - 1) <> "Black" Then
                    If Board(index - 1).Chars(2) <> "A" And Board(index - 1).Chars(2) <> "1" Then
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & Val(Board(index - 1).Chars(2)) - 1
                    ElseIf Board(index - 1).Chars(2) = "1" Then

                    Else
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index - 1).Chars(0) = Board(index).Chars(0) And Board(index - 1) <> "Black" Then
                    If Board(index - 1).Chars(2) = "A" Then

                    ElseIf Board(index - 1).Chars(2) = "9" Then
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & "A"
                    Else
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & Val(Board(index - 1).Chars(2)) + 1
                    End If
                End If

                If Board(index - gridWidth).Chars(0) <> Board(index).Chars(0) And Board(index - gridWidth) <> "Black" Then
                    If Board(index - gridWidth).Chars(2) <> "A" And Board(index - gridWidth).Chars(2) <> "1" Then
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & Val(Board(index - gridWidth).Chars(2)) - 1
                    ElseIf Board(index - gridWidth).Chars(2) = "1" Then

                    Else
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index - gridWidth).Chars(0) = Board(index).Chars(0) And Board(index - gridWidth) <> "Black" Then
                    If Board(index - gridWidth).Chars(2) = "A" Then

                    ElseIf Board(index - gridWidth).Chars(2) = "9" Then
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & "A"
                    Else
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & Val(Board(index - gridWidth).Chars(2)) + 1
                    End If
                End If

            End If

        ElseIf (index = (gridWidth * gridWidth) - gridWidth) Then   'Bottom Left Corner
            If Board(index + 1).Chars(2) = "A" Then
                numRight = 10
            Else
                numRight = getNumber(index + 1)
            End If
            If Board(index - gridWidth).Chars(2) = "A" Then
                numUp = 10
            Else
                numUp = getNumber(index - gridWidth)
            End If

            If Board(index).Chars(1) = "S" Then
                If (Board(index + 1).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numRight _
                  And Board(index + 1) <> "Black" Then
                    If (Board(index + 1).Chars(0) = "B") Then
                        Board(index + 1) = "R" & Board(index + 1).Chars(1) & Board(index + 1).Chars(2)
                    Else
                        Board(index + 1) = "B" & Board(index + 1).Chars(1) & Board(index + 1).Chars(2)
                    End If
                Else

                End If

                If (Board(index - gridWidth).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numUp _
                  And Board(index - gridWidth) <> "Black" Then
                    If (Board(index - gridWidth).Chars(0) = "B") Then
                        Board(index - gridWidth) = "R" & Board(index - gridWidth).Chars(1) _
                          & Board(index - gridWidth).Chars(2)
                    Else
                        Board(index - gridWidth) = "B" & Board(index - gridWidth).Chars(1) _
                          & Board(index - gridWidth).Chars(2)
                    End If
                End If
            Else

                If Board(index + 1).Chars(0) <> Board(index).Chars(0) And Board(index + 1) <> "Black" Then
                    If Board(index + 1).Chars(2) <> "A" And Board(index + 1).Chars(2) <> "1" Then
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & Val(Board(index + 1).Chars(2)) - 1
                    ElseIf Board(index + 1).Chars(2) = "1" Then

                    Else
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index + 1).Chars(0) = Board(index).Chars(0) And Board(index + 1) <> "Black" Then
                    If Board(index + 1).Chars(2) = "A" Then

                    ElseIf Board(index + 1).Chars(2) = "9" Then
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & "A"
                    Else
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & Val(Board(index + 1).Chars(2)) + 1
                    End If
                End If

                If Board(index - gridWidth).Chars(0) <> Board(index).Chars(0) And Board(index - gridWidth) <> "Black" Then
                    If Board(index - gridWidth).Chars(2) <> "A" And Board(index - gridWidth).Chars(2) <> "1" Then
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & Val(Board(index - gridWidth).Chars(2)) - 1
                    ElseIf Board(index - gridWidth).Chars(2) = "1" Then

                    Else
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index - gridWidth).Chars(0) = Board(index).Chars(0) And Board(index - gridWidth) <> "Black" Then
                    If Board(index - gridWidth).Chars(2) = "A" Then

                    ElseIf Board(index - gridWidth).Chars(2) = "9" Then
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & "A"
                    Else
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & Val(Board(index - gridWidth).Chars(2)) + 1
                    End If
                End If

            End If

        ElseIf (index Mod gridWidth = 0) Then   'Left Edge
            If Board(index + 1).Chars(2) = "A" Then
                numRight = 10
            Else
                numRight = getNumber(index + 1)
            End If
            If Board(index + gridWidth).Chars(2) = "A" Then
                numDown = 10
            Else
                numDown = getNumber(index + gridWidth)
            End If
            If Board(index - gridWidth).Chars(2) = "A" Then
                numUp = 10
            Else
                numUp = getNumber(index - gridWidth)
            End If

            If Board(index).Chars(1) = "S" Then
                If (Board(index + 1).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numRight _
                  And Board(index + 1) <> "Black" Then
                    If (Board(index + 1).Chars(0) = "B") Then
                        Board(index + 1) = "R" & Board(index + 1).Chars(1) & Board(index + 1).Chars(2)
                    Else
                        Board(index + 1) = "B" & Board(index + 1).Chars(1) & Board(index + 1).Chars(2)
                    End If
                Else

                End If

                If (Board(index + gridWidth).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numDown _
                  And Board(index + gridWidth) <> "Black" Then
                    If (Board(index + gridWidth).Chars(0) = "B") Then
                        Board(index + gridWidth) = "R" & Board(index + gridWidth).Chars(1) _
                          & Board(index + gridWidth).Chars(2)
                    Else
                        Board(index + gridWidth) = "B" & Board(index + gridWidth).Chars(1) _
                          & Board(index + gridWidth).Chars(2)
                    End If
                End If

                If (Board(index - gridWidth).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numUp _
                  And Board(index - gridWidth) <> "Black" Then
                    If (Board(index - gridWidth).Chars(0) = "B") Then
                        Board(index - gridWidth) = "R" & Board(index - gridWidth).Chars(1) _
                          & Board(index - gridWidth).Chars(2)
                    Else
                        Board(index - gridWidth) = "B" & Board(index - gridWidth).Chars(1) _
                          & Board(index - gridWidth).Chars(2)
                    End If
                End If
            Else
                If Board(index + 1).Chars(0) <> Board(index).Chars(0) And Board(index + 1) <> "Black" Then
                    If Board(index + 1).Chars(2) <> "A" And Board(index + 1).Chars(2) <> "1" Then
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & Val(Board(index + 1).Chars(2)) - 1
                    ElseIf Board(index + 1).Chars(2) = "1" Then

                    Else
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index + 1).Chars(0) = Board(index).Chars(0) And Board(index + 1) <> "Black" Then
                    If Board(index + 1).Chars(2) = "A" Then

                    ElseIf Board(index + 1).Chars(2) = "9" Then
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & "A"
                    Else
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & Val(Board(index + 1).Chars(2)) + 1
                    End If
                End If

                If Board(index - gridWidth).Chars(0) <> Board(index).Chars(0) And Board(index - gridWidth) <> "Black" Then
                    If Board(index - gridWidth).Chars(2) <> "A" And Board(index - gridWidth).Chars(2) <> "1" Then
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & Val(Board(index - gridWidth).Chars(2)) - 1
                    ElseIf Board(index - gridWidth).Chars(2) = "1" Then

                    Else
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index - gridWidth).Chars(0) = Board(index).Chars(0) And Board(index - gridWidth) <> "Black" Then
                    If Board(index - gridWidth).Chars(2) = "A" Then

                    ElseIf Board(index - gridWidth).Chars(2) = "9" Then
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & "A"
                    Else
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & Val(Board(index - gridWidth).Chars(2)) + 1
                    End If
                End If

                If Board(index + gridWidth).Chars(0) <> Board(index).Chars(0) And Board(index + gridWidth) <> "Black" Then
                    If Board(index + gridWidth).Chars(2) <> "A" And Board(index + gridWidth).Chars(2) <> "1" Then
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & Val(Board(index + gridWidth).Chars(2)) - 1
                    ElseIf Board(index + gridWidth).Chars(2) = "1" Then

                    Else
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index + gridWidth).Chars(0) = Board(index).Chars(0) And Board(index + gridWidth) <> "Black" Then
                    If Board(index + gridWidth).Chars(2) = "A" Then

                    ElseIf Board(index + gridWidth).Chars(2) = "9" Then
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & "A"
                    Else
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & Val(Board(index + gridWidth).Chars(2)) + 1
                    End If
                End If

            End If

        ElseIf (index Mod gridWidth = gridWidth - 1) Then   'Right Edge
            If Board(index - 1).Chars(2) = "A" Then
                numLeft = 10
            Else
                numLeft = getNumber(index - 1)
            End If
            If Board(index + gridWidth).Chars(2) = "A" Then
                numDown = 10
            Else
                numDown = getNumber(index + gridWidth)
            End If
            If Board(index - gridWidth).Chars(2) = "A" Then
                numUp = 10
            Else
                numUp = getNumber(index - gridWidth)
            End If

            If Board(index).Chars(1) = "S" Then

                If (Board(index - 1).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numLeft _
                  And Board(index - 1) <> "Black" Then
                    If (Board(index - 1).Chars(0) = "B") Then
                        Board(index - 1) = "R" & Board(index - 1).Chars(1) & Board(index - 1).Chars(2)
                    Else
                        Board(index - 1) = "B" & Board(index - 1).Chars(1) & Board(index - 1).Chars(2)
                    End If
                Else

                End If

                If (Board(index + gridWidth).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numDown _
                  And Board(index + gridWidth) <> "Black" Then
                    If (Board(index + gridWidth).Chars(0) = "B") Then
                        Board(index + gridWidth) = "R" & Board(index + gridWidth).Chars(1) _
                          & Board(index + gridWidth).Chars(2)
                    Else
                        Board(index + gridWidth) = "B" & Board(index + gridWidth).Chars(1) _
                          & Board(index + gridWidth).Chars(2)
                    End If
                End If

                If (Board(index - gridWidth).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numUp _
                  And Board(index - gridWidth) <> "Black" Then
                    If (Board(index - gridWidth).Chars(0) = "B") Then
                        Board(index - gridWidth) = "R" & Board(index - gridWidth).Chars(1) _
                          & Board(index - gridWidth).Chars(2)
                    Else
                        Board(index - gridWidth) = "B" & Board(index - gridWidth).Chars(1) _
                          & Board(index - gridWidth).Chars(2)
                    End If
                End If
            Else
                If Board(index - 1).Chars(0) <> Board(index).Chars(0) And Board(index - 1) <> "Black" Then
                    If Board(index - 1).Chars(2) <> "A" And Board(index - 1).Chars(2) <> "1" Then
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & Val(Board(index - 1).Chars(2)) - 1
                    ElseIf Board(index - 1).Chars(2) = "1" Then

                    Else
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index - 1).Chars(0) = Board(index).Chars(0) And Board(index - 1) <> "Black" Then
                    If Board(index - 1).Chars(2) = "A" Then

                    ElseIf Board(index - 1).Chars(2) = "9" Then
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & "A"
                    Else
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & Val(Board(index - 1).Chars(2)) + 1
                    End If
                End If

                If Board(index - gridWidth).Chars(0) <> Board(index).Chars(0) And Board(index - gridWidth) <> "Black" Then
                    If Board(index - gridWidth).Chars(2) <> "A" And Board(index - gridWidth).Chars(2) <> "1" Then
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & Val(Board(index - gridWidth).Chars(2)) - 1
                    ElseIf Board(index - gridWidth).Chars(2) = "1" Then

                    Else
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index - gridWidth).Chars(0) = Board(index).Chars(0) And Board(index - gridWidth) <> "Black" Then
                    If Board(index - gridWidth).Chars(2) = "A" Then

                    ElseIf Board(index - gridWidth).Chars(2) = "9" Then
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & "A"
                    Else
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & Val(Board(index - gridWidth).Chars(2)) + 1
                    End If
                End If

                If Board(index + gridWidth).Chars(0) <> Board(index).Chars(0) And Board(index + gridWidth) <> "Black" Then
                    If Board(index + gridWidth).Chars(2) <> "A" And Board(index + gridWidth).Chars(2) <> "1" Then
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & Val(Board(index + gridWidth).Chars(2)) - 1
                    ElseIf Board(index + gridWidth).Chars(2) = "1" Then

                    Else
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index + gridWidth).Chars(0) = Board(index).Chars(0) And Board(index + gridWidth) <> "Black" Then
                    If Board(index + gridWidth).Chars(2) = "A" Then

                    ElseIf Board(index + gridWidth).Chars(2) = "9" Then
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & "A"
                    Else
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & Val(Board(index + gridWidth).Chars(2)) + 1
                    End If
                End If

            End If

        ElseIf (index < gridWidth) Then   'Top Edge
            If Board(index - 1).Chars(2) = "A" Then
                numLeft = 10
            Else
                numLeft = getNumber(index - 1)
            End If
            If Board(index + gridWidth).Chars(2) = "A" Then
                numDown = 10
            Else
                numDown = getNumber(index + gridWidth)
            End If
            If Board(index + 1).Chars(2) = "A" Then
                numUp = 10
            Else
                numRight = getNumber(index + 1)
            End If

            If Board(index).Chars(1) = "S" Then
                If (Board(index + 1).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numRight _
                  And Board(index + 1) <> "Black" Then
                    If (Board(index + 1).Chars(0) = "B") Then
                        Board(index + 1) = "R" & Board(index + 1).Chars(1) & Board(index + 1).Chars(2)
                    Else
                        Board(index + 1) = "B" & Board(index + 1).Chars(1) & Board(index + 1).Chars(2)
                    End If
                Else

                End If

                If (Board(index - 1).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numLeft _
                  And Board(index - 1) <> "Black" Then
                    If (Board(index - 1).Chars(0) = "B") Then
                        Board(index - 1) = "R" & Board(index - 1).Chars(1) & Board(index - 1).Chars(2)
                    Else
                        Board(index - 1) = "B" & Board(index - 1).Chars(1) & Board(index - 1).Chars(2)
                    End If
                Else

                End If

                If (Board(index + gridWidth).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numDown _
                  And Board(index + gridWidth) <> "Black" Then
                    If (Board(index + gridWidth).Chars(0) = "B") Then
                        Board(index + gridWidth) = "R" & Board(index + gridWidth).Chars(1) _
                          & Board(index + gridWidth).Chars(2)
                    Else
                        Board(index + gridWidth) = "B" & Board(index + gridWidth).Chars(1) _
                          & Board(index + gridWidth).Chars(2)
                    End If
                End If

            Else
                If Board(index + 1).Chars(0) <> Board(index).Chars(0) And Board(index + 1) <> "Black" Then
                    If Board(index + 1).Chars(2) <> "A" And Board(index + 1).Chars(2) <> "1" Then
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & Val(Board(index + 1).Chars(2)) - 1
                    ElseIf Board(index + 1).Chars(2) = "1" Then

                    Else
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index + 1).Chars(0) = Board(index).Chars(0) And Board(index + 1) <> "Black" Then
                    If Board(index + 1).Chars(2) = "A" Then

                    ElseIf Board(index + 1).Chars(2) = "9" Then
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & "A"
                    Else
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & Val(Board(index + 1).Chars(2)) + 1
                    End If
                End If

                If Board(index - 1).Chars(0) <> Board(index).Chars(0) And Board(index - 1) <> "Black" Then
                    If Board(index - 1).Chars(2) <> "A" And Board(index - 1).Chars(2) <> "1" Then
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & Val(Board(index - 1).Chars(2)) - 1
                    ElseIf Board(index - 1).Chars(2) = "1" Then

                    Else
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index - 1).Chars(0) = Board(index).Chars(0) And Board(index - 1) <> "Black" Then
                    If Board(index - 1).Chars(2) = "A" Then

                    ElseIf Board(index - 1).Chars(2) = "9" Then
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & "A"
                    Else
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & Val(Board(index - 1).Chars(2)) + 1
                    End If
                End If

                If Board(index + gridWidth).Chars(0) <> Board(index).Chars(0) And Board(index + gridWidth) <> "Black" Then
                    If Board(index + gridWidth).Chars(2) <> "A" And Board(index + gridWidth).Chars(2) <> "1" Then
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & Val(Board(index + gridWidth).Chars(2)) - 1
                    ElseIf Board(index + gridWidth).Chars(2) = "1" Then

                    Else
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index + gridWidth).Chars(0) = Board(index).Chars(0) And Board(index + gridWidth) <> "Black" Then
                    If Board(index + gridWidth).Chars(2) = "A" Then

                    ElseIf Board(index + gridWidth).Chars(2) = "9" Then
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & "A"
                    Else
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & Val(Board(index + gridWidth).Chars(2)) + 1
                    End If
                End If

            End If

        ElseIf (index >= (gridWidth * gridWidth) - gridWidth) Then   'Bottom Edge
            If Board(index - 1).Chars(2) = "A" Then
                numLeft = 10
            Else
                numLeft = getNumber(index - 1)
            End If
            If Board(index - gridWidth).Chars(2) = "A" Then
                numUp = 10
            Else
                numUp = getNumber(index - gridWidth)
            End If
            If Board(index + 1).Chars(2) = "A" Then
                numUp = 10
            Else
                numRight = getNumber(index + 1)
            End If

            If Board(index).Chars(1) = "S" Then
                If (Board(index + 1).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numRight _
                  And Board(index + 1) <> "Black" Then
                    If (Board(index + 1).Chars(0) = "B") Then
                        Board(index + 1) = "R" & Board(index + 1).Chars(1) & Board(index + 1).Chars(2)
                    Else
                        Board(index + 1) = "B" & Board(index + 1).Chars(1) & Board(index + 1).Chars(2)
                    End If
                Else

                End If

                If (Board(index - gridWidth).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numUp _
                  And Board(index - gridWidth) <> "Black" Then
                    If (Board(index - gridWidth).Chars(0) = "B") Then
                        Board(index - gridWidth) = "R" & Board(index - gridWidth).Chars(1) _
                          & Board(index - gridWidth).Chars(2)
                    Else
                        Board(index - gridWidth) = "B" & Board(index - gridWidth).Chars(1) _
                          & Board(index - gridWidth).Chars(2)
                    End If
                End If

                If (Board(index - 1).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numLeft _
                  And Board(index - 1) <> "Black" Then
                    If (Board(index - 1).Chars(0) = "B") Then
                        Board(index - 1) = "R" & Board(index - 1).Chars(1) & Board(index - 1).Chars(2)
                    Else
                        Board(index - 1) = "B" & Board(index - 1).Chars(1) & Board(index - 1).Chars(2)
                    End If
                Else

                End If

            Else
                If Board(index + 1).Chars(0) <> Board(index).Chars(0) And Board(index + 1) <> "Black" Then
                    If Board(index + 1).Chars(2) <> "A" And Board(index + 1).Chars(2) <> "1" Then
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & Val(Board(index + 1).Chars(2)) - 1
                    ElseIf Board(index + 1).Chars(2) = "1" Then

                    Else
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index + 1).Chars(0) = Board(index).Chars(0) And Board(index + 1) <> "Black" Then
                    If Board(index + 1).Chars(2) = "A" Then

                    ElseIf Board(index + 1).Chars(2) = "9" Then
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & "A"
                    Else
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & Val(Board(index + 1).Chars(2)) + 1
                    End If
                End If

                If Board(index - 1).Chars(0) <> Board(index).Chars(0) And Board(index - 1) <> "Black" Then
                    If Board(index - 1).Chars(2) <> "A" And Board(index - 1).Chars(2) <> "1" Then
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & Val(Board(index - 1).Chars(2)) - 1
                    ElseIf Board(index - 1).Chars(2) = "1" Then

                    Else
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index - 1).Chars(0) = Board(index).Chars(0) And Board(index - 1) <> "Black" Then
                    If Board(index - 1).Chars(2) = "A" Then

                    ElseIf Board(index - 1).Chars(2) = "9" Then
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & "A"
                    Else
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & Val(Board(index - 1).Chars(2)) + 1
                    End If
                End If

                If Board(index - gridWidth).Chars(0) <> Board(index).Chars(0) And Board(index - gridWidth) <> "Black" Then
                    If Board(index - gridWidth).Chars(2) <> "A" And Board(index - gridWidth).Chars(2) <> "1" Then
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & Val(Board(index - gridWidth).Chars(2)) - 1
                    ElseIf Board(index - gridWidth).Chars(2) = "1" Then

                    Else
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index - gridWidth).Chars(0) = Board(index).Chars(0) And Board(index - gridWidth) <> "Black" Then
                    If Board(index - gridWidth).Chars(2) = "A" Then

                    ElseIf Board(index - gridWidth).Chars(2) = "9" Then
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & "A"
                    Else
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & Val(Board(index - gridWidth).Chars(2)) + 1
                    End If
                End If

            End If

        Else   'Middle
            If Board(index + 1).Chars(2) = "A" Then
                numRight = 10
            Else
                numRight = getNumber(index + 1)
            End If
            If Board(index - 1).Chars(2) = "A" Then
                numLeft = 10
            Else
                numLeft = getNumber(index - 1)
            End If
            If Board(index + gridWidth).Chars(2) = "A" Then
                numDown = 10
            Else
                numDown = getNumber(index + gridWidth)
            End If
            If Board(index - gridWidth).Chars(2) = "A" Then
                numUp = 10
            Else
                numUp = getNumber(index - gridWidth)
            End If

            If Board(index).Chars(1) = "S" Then
                If (Board(index + 1).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numRight _
                  And Board(index + 1) <> "Black" Then
                    If (Board(index + 1).Chars(0) = "B") Then
                        Board(index + 1) = "R" & Board(index + 1).Chars(1) & Board(index + 1).Chars(2)
                    Else
                        Board(index + 1) = "B" & Board(index + 1).Chars(1) & Board(index + 1).Chars(2)
                    End If
                Else

                End If

                If (Board(index - 1).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numLeft _
                  And Board(index - 1) <> "Black" Then
                    If (Board(index - 1).Chars(0) = "B") Then
                        Board(index - 1) = "R" & Board(index - 1).Chars(1) & Board(index - 1).Chars(2)
                    Else
                        Board(index - 1) = "B" & Board(index - 1).Chars(1) & Board(index - 1).Chars(2)
                    End If
                Else

                End If

                If (Board(index + gridWidth).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numDown _
                  And Board(index + gridWidth) <> "Black" Then
                    If (Board(index + gridWidth).Chars(0) = "B") Then
                        Board(index + gridWidth) = "R" & Board(index + gridWidth).Chars(1) _
                          & Board(index + gridWidth).Chars(2)
                    Else
                        Board(index + gridWidth) = "B" & Board(index + gridWidth).Chars(1) _
                          & Board(index + gridWidth).Chars(2)
                    End If
                End If

                If (Board(index - gridWidth).Chars(0) <> Board(index).Chars(0)) And numCurrent >= numUp _
                  And Board(index - gridWidth) <> "Black" Then
                    If (Board(index - gridWidth).Chars(0) = "B") Then
                        Board(index - gridWidth) = "R" & Board(index - gridWidth).Chars(1) _
                          & Board(index - gridWidth).Chars(2)
                    Else
                        Board(index - gridWidth) = "B" & Board(index - gridWidth).Chars(1) _
                          & Board(index - gridWidth).Chars(2)
                    End If
                End If

            Else
                If Board(index + 1).Chars(0) <> Board(index).Chars(0) And Board(index + 1) <> "Black" Then
                    If Board(index + 1).Chars(2) <> "A" And Board(index + 1).Chars(2) <> "1" Then
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & Val(Board(index + 1).Chars(2)) - 1
                    ElseIf Board(index + 1).Chars(2) = "1" Then

                    Else
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index + 1).Chars(0) = Board(index).Chars(0) And Board(index + 1) <> "Black" Then
                    If Board(index + 1).Chars(2) = "A" Then

                    ElseIf Board(index + 1).Chars(2) = "9" Then
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & "A"
                    Else
                        Board(index + 1) = Board(index + 1).Chars(0) & Board(index + 1).Chars(1) & Val(Board(index + 1).Chars(2)) + 1
                    End If
                End If

                If Board(index - 1).Chars(0) <> Board(index).Chars(0) And Board(index - 1) <> "Black" Then
                    If Board(index - 1).Chars(2) <> "A" And Board(index - 1).Chars(2) <> "1" Then
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & Val(Board(index - 1).Chars(2)) - 1
                    ElseIf Board(index - 1).Chars(2) = "1" Then

                    Else
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index - 1).Chars(0) = Board(index).Chars(0) And Board(index - 1) <> "Black" Then
                    If Board(index - 1).Chars(2) = "A" Then

                    ElseIf Board(index - 1).Chars(2) = "9" Then
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & "A"
                    Else
                        Board(index - 1) = Board(index - 1).Chars(0) & Board(index - 1).Chars(1) & Val(Board(index - 1).Chars(2)) + 1
                    End If
                End If

                If Board(index - gridWidth).Chars(0) <> Board(index).Chars(0) And Board(index - gridWidth) <> "Black" Then
                    If Board(index - gridWidth).Chars(2) <> "A" And Board(index - gridWidth).Chars(2) <> "1" Then
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & Val(Board(index - gridWidth).Chars(2)) - 1
                    ElseIf Board(index - gridWidth).Chars(2) = "1" Then

                    Else
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index - gridWidth).Chars(0) = Board(index).Chars(0) And Board(index - gridWidth) <> "Black" Then
                    If Board(index - gridWidth).Chars(2) = "A" Then

                    ElseIf Board(index - gridWidth).Chars(2) = "9" Then
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & "A"
                    Else
                        Board(index - gridWidth) = Board(index - gridWidth).Chars(0) _
                            & Board(index - gridWidth).Chars(1) & Val(Board(index - gridWidth).Chars(2)) + 1
                    End If
                End If

                If Board(index + gridWidth).Chars(0) <> Board(index).Chars(0) And Board(index + gridWidth) <> "Black" Then
                    If Board(index + gridWidth).Chars(2) <> "A" And Board(index + gridWidth).Chars(2) <> "1" Then
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & Val(Board(index + gridWidth).Chars(2)) - 1
                    ElseIf Board(index + gridWidth).Chars(2) = "1" Then

                    Else
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & CStr(9)
                    End If
                ElseIf Board(index + gridWidth).Chars(0) = Board(index).Chars(0) And Board(index + gridWidth) <> "Black" Then
                    If Board(index + gridWidth).Chars(2) = "A" Then

                    ElseIf Board(index + gridWidth).Chars(2) = "9" Then
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & "A"
                    Else
                        Board(index + gridWidth) = Board(index + gridWidth).Chars(0) _
                            & Board(index + gridWidth).Chars(1) & Val(Board(index + gridWidth).Chars(2)) + 1
                    End If
                End If

            End If

        End If
    End Sub

    Private Function getNumber(ByVal index As Integer)
        Dim value As Integer

        value = Val(Board(index).Chars(2))

        Return value
    End Function
    Private Sub onClose() Handles Me.FormClosed
        MainMenu.Show()
    End Sub
End Class