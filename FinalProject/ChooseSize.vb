﻿Public Class ChooseSize
    Public num As UInteger = 0
    Private Sub cbxBoardSize_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbxBoardSize.SelectedIndexChanged
        btnConfirm.Visible = True
        If cbxBoardSize.SelectedItem = "Small (3 x 3)" Then
            picSize.BackgroundImage = My.Resources.SwordWhite
        ElseIf cbxBoardSize.SelectedItem = "Medium (4 x 4)" Then
            picSize.BackgroundImage = My.Resources.CrossedSwordsWhite
        ElseIf cbxBoardSize.SelectedItem = "Large (5 x 5)" Then
            picSize.BackgroundImage = My.Resources.ShieldWhite
        ElseIf cbxBoardSize.SelectedItem = "Endurance (10 x 10)" Then
            picSize.BackgroundImage = My.Resources.Clock
        End If
    End Sub

    Private Sub btnConfirm_Click(sender As System.Object, e As System.EventArgs) Handles btnConfirm.Click
        If cbxBoardSize.SelectedItem = "Small (3 x 3)" Then
            GameBoard.Size = New Point(600, 500)
            num = 3
        ElseIf cbxBoardSize.SelectedItem = "Medium (4 x 4)" Then
            GameBoard.Size = New Point(700, 550)
            num = 4
        ElseIf cbxBoardSize.SelectedItem = "Large (5 x 5)" Then
            GameBoard.Size = New Point(800, 600)
            num = 5
        ElseIf cbxBoardSize.SelectedItem = "Endurance (10 x 10)" Then
            GameBoard.Size = New Point(1100, 900)
            num = 10
        End If
        GameBoard.Show()
        Me.Close()
    End Sub
End Class